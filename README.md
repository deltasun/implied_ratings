# IMPLIED RATINGS FROM BOND AND CDS SPREADS FOR SOVEREIGN EMITTERS
**joint work with Tommaso Colozza, Stefano Marmi, Aldo Nassigh**

**reboot 2019**

computing sovereign ratings implied by CDS and BOND spreads via a simple "analytic" model and via some Machine Learning algorithm (SVM and XGB).
The analysis is detailed in the paper https://papers.ssrn.com/sol3/papers.cfm?abstract_id=2512238


the project is organized as follows:

* a `main.R` script which is kind of workflow script, where each block has some specific functionality
* a `./R/fun.R` script with all utility functions
* other scripts governing the implied ratings computation with different models

data are not available in this repo.
